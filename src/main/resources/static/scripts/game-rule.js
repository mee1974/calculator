/**
 *
 * @link https://www.hbuecx.club/exercise
 *
 * @author chenxin
 *
 * @description 游戏计时器，使用时只需要规定每分钟消耗多少积分prescore、最长玩游戏时间max_time即可
 *
 */

//用于存储用户还有多少积分
var resscore = 0;

//用于规定每分钟消耗多少分
var prescore = 5;

//用于存储用户积分最多可玩多长时间
var allow_time = 0;

/**
 * 获取用户积分
 */
$.ajax({
    type: "post",
    url: "/getscoregame/",
    dataType: "json",
    async: false, //同步
    data: {
        userId: userid
    },
    success: function(data) {
        resscore = data.score_game;
        if (resscore < prescore){
            alert("剩余积分"+resscore+"分，积分不足哦，快去考试获取积分吧！");
            window.location.href="/exam/";
        } else{
            //绘制计时计分信息
            var gameinfo = '<div class="game-info-main">' +
                '<div class="info-module">' +
                '<p class="use-time">' +
                '您已用时：<span><strong>00:00:00</strong></span>' +
                '</p>' +
                '<p class="use-score">' +
                '累计消耗：<span><strong>0分</strong></span>' +
                '</p>' +
                '</div>' +
                '<div class="game-exit">' +
                '<a href="javascript:void(0);">退出游戏</a>' +
                '</div>' +
                '</div>' +
                '<div class="rule"></div>';

            $(".game-info-module").html(gameinfo);
        }
        allow_time = parseInt(resscore / prescore) * 60;
    },
    error: function(data) {

        alert("积分信息获取失败！即将返回……");

        window.location.href = "/game/";
    }
});

//页面打开即开始页面计时
var totle = setInterval("caculatortime()", 1000);

//游戏时间显示块
var use_time = $(".use-time strong");

//消耗分数显示块
var use_score = $(".use-score strong");

//用于存储消耗的分数
var score = 0;

//用于存储游戏时间信息
var timemsg="";

//用于存储游戏时间
var totletime = 0;
var minutes = 0;
var seconds = 0;

//用于存储系统最长可玩游戏多长时间
var max_time = 20 * 60;

//退出游戏的原因
var exit_msg = "";

/**
 * 计算游戏总时间
 */
function caculatortime() {

    if(totletime < max_time && totletime < allow_time) { //如果没有到最长游戏时间
        ++totletime;
        minutes = Math.floor(totletime / 60);
        seconds = Math.floor(totletime % 60);

        //计算积分
        if (0 == seconds) {
            score = minutes * prescore;
        } else{
            score = (minutes + 1) * prescore;
        }

        //刷新游戏时间
        if(minutes < 10) {
            if(seconds < 10) {
                timemsg = "00:0" + minutes + ":0" + seconds;
            } else {
                timemsg = "00:0" + minutes + ":" + seconds;
            }
        } else {
            if(seconds < 10) {
                timemsg = "00:" + minutes + ":0" + seconds;
            } else {
                timemsg = "00:" + minutes + ":" + seconds;
            }
        }

        use_time.text(timemsg);
        use_score.text(score + "分");
    } else if (totletime == allow_time || totletime > allow_time){ //如果积分耗尽

        exit_msg = "您的积分不足，系统已自动终止游戏！";

        commitgame(exit_msg);

    } else { //如果达到游戏最长时间

        exit_msg = "玩了这么长时间，休息一会儿吧！";

        commitgame(exit_msg);
    }
}

/**
 * 退出游戏
 */
$(".game-exit").click(function() {
    exit_msg = "";
    commitgame(exit_msg);
});

/**
 * 结束游戏，向后台发送数据
 *
 * @param msg
 */
function commitgame(exit_msg){

    //清除计时器
    clearInterval(totle);

    //结束游戏，向后台传送数据
    $.ajax({
        type: "post",
        url: "/quitgame/",
        dataType: "json",
        async: false, //同步
        data: {
            userId: userid,
            expendScore: score
        },
        success: function(data) {
            if ("" != exit_msg){
                alert(exit_msg);
            }
            //跳转至积分记录页
            window.location.href = "/game/";
        },
        error: function(data) {
            alert("数据传输失败！");
        }
    });
}